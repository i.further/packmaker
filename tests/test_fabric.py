# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from packmaker.download import HttpDownloader
from packmaker.fabric import FabricModLoader
from packmaker.framew.log import getlog

##############################################################################


def setup_module(module):
    log = getlog()
    log.setLevel('DEBUG')


class Base_TestFabric (object):
    mc_version = '__undefined__'
    fabric_version = '__undefined__'

    @pytest.fixture(scope='function')
    def fabric_obj(self, tmp_path):
        downloader = HttpDownloader(tmp_path)
        fabric = FabricModLoader(self.mc_version, self.fabric_version, downloader)
        yield fabric

    def test_get_mainclass(self, fabric_obj):
        mainclass = fabric_obj.get_mainclass()
        assert mainclass is not None

    def test_get_launch_arguments(self, fabric_obj):
        launchargs = fabric_obj.get_launch_arguments()
        assert launchargs is None

    def test_get_server_jar_filename(self, fabric_obj):
        filename = fabric_obj.get_server_jar_filename()
        assert filename is not None

    def test_get_client_libraries(self, fabric_obj):
        dependents = fabric_obj.get_libraries()
        assert dependents is not None
        assert len(dependents) > 0

    def test_get_server_libraries(self, fabric_obj):
        dependents = fabric_obj.get_libraries(side='server')
        assert dependents is not None
        assert len(dependents) > 0

    @pytest.mark.slow
    def test_install_client(self, fabric_obj, tmp_path):
        cache_loc = tmp_path / 'cache'
        inst_loc = tmp_path / 'install'
        fabric_obj.install(cache_loc, inst_loc, 'client')

    @pytest.mark.slow
    def test_install_server(self, fabric_obj, tmp_path):
        cache_loc = tmp_path / 'cache'
        inst_loc = tmp_path / 'install'
        fabric_obj.install(cache_loc, inst_loc, 'server')

##############################################################################


class TestFabric_1144_0108 (Base_TestFabric):
    mc_version = '1.14.4'
    fabric_version = '0.10.8'


class TestFabric_1152_0108 (Base_TestFabric):
    mc_version = '1.15.2'
    fabric_version = '0.10.8'


class TestFabric_1164_0108 (Base_TestFabric):
    mc_version = '1.16.4'
    fabric_version = '0.10.8'


##############################################################################
# THE END
