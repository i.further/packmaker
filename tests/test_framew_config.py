# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
import os
import tempfile

from packmaker.framew.config import Config

##############################################################################

reference_config = """
[sectiona]
configparam1 = configvalue1
configparam2 = configvalue2

multiline = this is a long parameter\
  that continues on a second line

interpolated1 = this value is ${configparam1}
interpolated3 = this value does not match ${configparam5}

environment = ${ENV_INTERP}

[nonconfigsectionb]
the quick brown fox jumped over the lazy dog.

[sectionb]
configparam1 = configvalue3
configparam4 = configvalue4

interpolated2 = this value is ${sectiona::configparam2}

password = this is a secret

[nonconfigsectiona]
the quick brown fox jumped over the lazy dog.
"""

reference_sections = ['DEFAULT', 'sectiona', 'sectionb']

reference_environment_val = 'env interpolated value'


class TestConfig (object):

    def generate_config(self, config_string):
        os.environ['ENV_INTERP'] = reference_environment_val
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write(config_string)
            f.flush()
            config = Config(filenames=[f.name],
                            skipped_sections=['nonconfigsectiona', 'nonconfigsectionb'],
                            censored_params=['sectionb::password'])
            return config

    def test_get(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::configparam1') == 'configvalue1'
        assert config.get('sectiona::configparam2') == 'configvalue2'
        assert config.get('sectionb::configparam1') == 'configvalue3'
        assert config.get('sectionb::configparam4') == 'configvalue4'

    def test_get_multiline(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::multiline') == 'this is a long parameter  that continues on a second line'

    def test_get_interpolated(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::interpolated1') == 'this value is configvalue1'
        assert config.get('sectionb::interpolated2') == 'this value is configvalue2'

    def test_get_interpolated_missing(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::interpolated3') == 'this value does not match '

    def test_get_envvar(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::environment') == reference_environment_val

    def test_get_sections(self):
        config = self.generate_config(reference_config)
        sections = config.get_sections()
        assert len(sections) == len(reference_sections)
        for sec in reference_sections:
            assert sec in sections

    def test_get_section(self):
        config = self.generate_config(reference_config)
        for sec in reference_sections:
            config.section(sec)

    def test_missing_param(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::configparam1') == 'configvalue1'
        assert config.get('sectiona::configparamX') is None

    def test_missing_section(self):
        config = self.generate_config(reference_config)
        assert config.get('sectiona::configparam1') == 'configvalue1'
        assert config.get('sectionX::configparam1') is None

    def test_dump_config(self):
        config = self.generate_config(reference_config)
        f = io.StringIO()
        config.dump_config(f)
        out = f.getvalue()
        assert '[sectiona]\n' in out
        assert 'configparam1 = configvalue1' in out
        assert 'configparam2 = configvalue2' in out

    def test_dump_config_censored(self):
        config = self.generate_config(reference_config)
        f = io.StringIO()
        config.dump_config(f)
        out = f.getvalue()
        assert 'this is a secret' not in out
        assert 'password = xxxxxxx' in out

    def test_nonconfig_section(self):
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write(reference_config)
            f.flush()
            config = Config(filenames=[f.name], skipped_sections=['nonconfigsectiona', 'nonconfigsectionb'])
            data = config.read_nonconfig_section('nonconfigsectiona')
            assert data == ['the quick brown fox jumped over the lazy dog.']

            data = config.read_nonconfig_section('nonconfigsectionb')
            assert data == ['the quick brown fox jumped over the lazy dog.', '']

    def test_nonconfig_section_missing(self):
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write(reference_config)
            f.flush()
            config = Config(filenames=[f.name], skipped_sections=['nonconfigsection'])
            data = config.read_nonconfig_section('missing')
            assert data == []

    def test_config_paths(self):
        config = Config()
        paths = config._default_config_paths()
        assert len(paths) > 1
        assert paths[0] == '.'

    def test_config_paths_windows(self):
        config = Config()
        paths = config._default_config_paths(system='Windows')
        assert len(paths) > 1
        assert paths[0] == '.'
        assert '~\\AppData\\Roaming' in paths

        assert '~/Library/Application Support' not in paths
        assert '/usr/local/etc' not in paths
        assert '/etc' not in paths

        os.environ['APPDATA'] = 'my-custom-path'
        paths = config._default_config_paths(system='Windows')
        assert 'my-custom-path' in paths

    def test_config_paths_darwin(self):
        config = Config()
        paths = config._default_config_paths(system='Darwin')
        assert len(paths) > 1
        assert paths[0] == '.'
        assert '~/.config' in paths
        assert '~/Library/Application Support' in paths
        assert '/usr/local/etc' in paths
        assert '/etc' in paths

        assert '~\\AppData\\Roaming' not in paths

        os.environ['XDG_CONFIG_HOME'] = 'my-custom-path'
        paths = config._default_config_paths(system='Darwin')
        assert 'my-custom-path' in paths

    def test_config_paths_unix(self):
        config = Config()
        paths = config._default_config_paths(system='linux')
        assert len(paths) > 1
        assert paths[0] == '.'
        assert '~/.config' in paths
        assert '/usr/local/etc' in paths
        assert '/etc' in paths

        assert '~\\AppData\\Roaming' not in paths
        assert '~/Library/Application Support' not in paths

        os.environ['XDG_CONFIG_HOME'] = 'my-custom-path'
        paths = config._default_config_paths(system='linux')
        assert 'my-custom-path' in paths


##############################################################################
# THE END
