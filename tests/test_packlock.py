# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
import os
import tempfile

from packmaker import packdef, packlock
from packmaker.framew.application import OperationError

##############################################################################


class TestPackLock (object):

    def generate_packlock(self, packlock_string):
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write(packlock_string)
            f.flush()
            lock = packlock.PackLock([f.name])
            lock.load()
            return lock

    def test_lockformat_version0(self):
        lock = self.generate_packlock(packlock_version0)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_metadata()) is False  # empty
        assert bool(lock.get_all_mods()) is True  # not empty
        assert bool(lock.get_all_resourcepacks()) is False  # empty

    def test_lockformat_version1(self):
        lock = self.generate_packlock(packlock_version1)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not empty
        assert bool(lock.get_all_resourcepacks()) is True  # not empty
        assert isinstance(lock.get_mod('jei'), packlock.LockedElement)
        assert isinstance(lock.get_resourcepack('faithful-32x'), packlock.LockedElement)

    def test_invalidjson(self):
        with pytest.raises(OperationError):
            self.generate_packlock('{ invalid json data here ]')

    def test_invalidversion(self):
        with pytest.raises(OperationError):
            self.generate_packlock('{ "version": "X" }')

    def test_no_resolutiontype(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "jei" : {
                 "fileId" : 2847112,
                 "fileName" : "jei_1.12.2-4.15.0.293.jar",
                 "name" : "Just Enough Items (JEI)"
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not empty
        assert isinstance(lock.get_mod('jei'), packlock.LockedElement)

    def test_invalid_resolutiontype(self):
        with pytest.raises(Exception):
            self.generate_packlock("""
            {
               "resolutions" : {
                  "jei" : {
                     "fileId" : 2847112,
                     "fileName" : "jei_1.12.2-4.15.0.293.jar",
                     "name" : "Just Enough Items (JEI)",
                     "type" : "invalid type here"
                  }
               },
               "version" : "1"
            }
            """)

    def test_metadata_oneauthor(self):
        lock = self.generate_packlock("""
        {
           "metadata" : {
              "authors" : "mcrewson"
           },
           "resolutions": {},
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert lock.get_metadata('authors') == 'mcrewson'

    def test_metadata_multipleauthors(self):
        lock = self.generate_packlock("""
        {
           "metadata" : {
              "authors" : [ "mcrewson", "routhinator" ]
           },
           "resolutions": {},
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert lock.get_metadata('authors') == ['mcrewson', 'routhinator']

    def test_metadata_envvars(self):
        os.environ['EXAMPLEVAR'] = 'example'
        lock = self.generate_packlock("""
        {
           "metadata" : {
              "name" : "${EXAMPLEVAR}"
           },
           "resolutions": {},
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert lock.get_metadata('name') == 'example'

    def test_metadata_envvar_missing(self):
        with pytest.raises(OperationError):
            self.generate_packlock("""
            {
               "metadata" : {
                  "name" : "${MISSINGVAR}"
               },
               "resolutions": {},
               "version" : "1"
            }
            """)

    def test_metadata_envvar_default(self):
        lock = self.generate_packlock("""
        {
           "metadata" : {
              "name" : "${MISSINGVAR:-defaultvalue}"
           },
           "resolutions": {},
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert lock.get_metadata('name') == 'defaultvalue'

    def test_clientonly_mods(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "jei" : {
                 "name" : "Just Enough Items (JEI)"
              },
              "servermod": {
                 "name" : "some serveronly mod",
                 "serveronly" : true
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not  empty
        for mod in lock.yield_clientonly_mods():
            assert mod.slug != 'servermod'

    def test_clientonly_optional_mods(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "jei" : {
                 "name" : "Just Enough Items (JEI)"
              },
              "servermod": {
                 "name" : "some serveronly mod",
                 "optional" : true
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not  empty
        for mod in lock.yield_clientonly_mods():
            assert mod.slug != 'servermod'

    def test_serveronly_mods(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "jei" : {
                 "name" : "Just Enough Items (JEI)"
              },
              "clientmod": {
                 "name" : "some clientonly mod",
                 "clientonly" : true
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not  empty
        for mod in lock.yield_serveronly_mods():
            assert mod.slug != 'clientmod'

    def test_serveronly_optional_mods(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "jei" : {
                 "name" : "Just Enough Items (JEI)"
              },
              "clientmod": {
                 "name" : "some clientonly mod",
                 "optional" : true
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        assert bool(lock.get_all_mods()) is True  # not  empty
        for mod in lock.yield_serveronly_mods():
            assert mod.slug != 'clientmod'

    def test_rawurl_mods(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {
              "rawmod" : {
                 "name" : null,
                 "downloadUrl": "https://www.somesite.com/raw/url/mod.jar",
                 "fileName": "mod.jar"
              }
           },
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        mod = lock.get_mod('rawmod')
        assert mod.downloadUrl == "https://www.somesite.com/raw/url/mod.jar"
        assert mod.fileName == "mod.jar"
        assert mod.projectId is None

    def test_clientonly_files(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {},
           "files" : [
              {
                 "location" : "src/"
              },
              {
                 "location" : "src2/",
                 "serveronly": true
              }
           ],
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        for files in lock.yield_clientonly_files():
            assert files['location'] != 'src2/'

    def test_serveronly_files(self):
        lock = self.generate_packlock("""
        {
           "resolutions" : {},
           "files" : [
              {
                 "location" : "src/"
              },
              {
                 "location" : "src2/",
                 "clientonly": true
              }
           ],
           "version" : "1"
        }
        """)
        assert isinstance(lock, packlock.PackLock)
        for files in lock.yield_serveronly_files():
            assert files['location'] != 'src2/'

    def test_add_files(self):
        lock = packlock.PackLock(None)
        lock.add_files({'location': 'src/'})
        assert len(lock.files) == 1

    def test_add_extraopt(self):
        extradata = {'mydata': 'value'}
        lock = packlock.PackLock(None)
        lock.add_extraopt('someopt', extradata)
        assert lock.get_extraopt('someopt') == extradata

    def test_metadata_from_packdef(self):
        pack = None
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write("""
---
name: examplepack
title: Example Pack
version: 1.2.3
authors:
  - mcrewson
minecraft: 1.12.2
""")
            f.flush()
            pack = packdef.PackDefinition([f.name])
            pack.load()
        assert isinstance(pack, packdef.PackDefinition)
        lock = packlock.PackLock(None)
        lock.set_all_metadata(pack)
        assert lock.get_metadata('name') == 'examplepack'
        assert lock.get_metadata('title') == 'Example Pack'
        assert lock.get_metadata('version') == '1.2.3'
        assert lock.get_metadata('authors') == ['mcrewson']
        assert lock.get_metadata('minecraft_version') == '1.12.2'


##############################################################################

packlock_version0 = """
{
   "jei" : {
      "author" : "mezz",
      "downloadUrl" : "https://files.forgecdn.net/files/2684/645/jei_1.12.2-4.15.0.269.jar",
      "fileId" : 2684645,
      "fileName" : "jei_1.12.2-4.15.0.269.jar",
      "fileNameOnDisk" : "jei_1.12.2-4.15.0.269.jar",
      "name" : "Just Enough Items (JEI)",
      "projectId" : 238222,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/jei"
   },
   "journeymap" : {
      "author" : "techbrew",
      "downloadUrl" : "https://files.forgecdn.net/files/2682/920/journeymap-1.12.2-5.5.4.jar",
      "fileId" : 2682920,
      "fileName" : "journeymap-1.12.2-5.5.4",
      "fileNameOnDisk" : "journeymap-1.12.2-5.5.4.jar",
      "name" : "JourneyMap",
      "projectId" : 32274,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/journeymap"
   },
   "mantle" : {
      "author" : "mDiyo",
      "downloadUrl" : "https://files.forgecdn.net/files/2671/124/Mantle-1.12-1.3.3.42.jar",
      "fileId" : 2671124,
      "fileName" : "Mantle-1.12-1.3.3.42.jar",
      "fileNameOnDisk" : "Mantle-1.12-1.3.3.42.jar",
      "name" : "Mantle",
      "projectId" : 74924,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/mantle"
   },
   "the-one-probe" : {
      "author" : "McJty",
      "downloadUrl" : "https://files.forgecdn.net/files/2667/280/theoneprobe-1.12-1.4.28.jar",
      "fileId" : 2667280,
      "fileName" : "TheOneProbe - 1.12-1.4.28",
      "fileNameOnDisk" : "theoneprobe-1.12-1.4.28.jar",
      "name" : "The One Probe",
      "projectId" : 245211,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/the-one-probe"
   },
   "tinkers-construct" : {
      "author" : "mDiyo",
      "downloadUrl" : "https://files.forgecdn.net/files/2687/590/TConstruct-1.12.2-2.12.0.120.jar",
      "fileId" : 2687590,
      "fileName" : "TConstruct-1.12.2-2.12.0.120.jar",
      "fileNameOnDisk" : "TConstruct-1.12.2-2.12.0.120.jar",
      "name" : "Tinkers Construct",
      "projectId" : 74072,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/tinkers-construct"
   },
   "tinkers-tool-leveling" : {
      "author" : "bonusboni",
      "downloadUrl" : "https://files.forgecdn.net/files/2630/860/TinkerToolLeveling-1.12.2-1.1.0.jar",
      "fileId" : 2630860,
      "fileName" : "TinkerToolLeveling-1.12.2-1.1.0.jar",
      "fileNameOnDisk" : "TinkerToolLeveling-1.12.2-1.1.0.jar",
      "name" : "Tinkers' Tool Leveling",
      "projectId" : 250957,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/tinkers-tool-leveling"
   },
   "top-addons" : {
      "author" : "DrManganese",
      "downloadUrl" : "https://files.forgecdn.net/files/2642/149/topaddons-1.12.2-1.10.1.jar",
      "fileId" : 2642149,
      "fileName" : "topaddons-1.12.2-1.10.1.jar",
      "fileNameOnDisk" : "topaddons-1.12.2-1.10.1.jar",
      "name" : "TOP Addons",
      "projectId" : 247111,
      "website" : "https://www.curseforge.com/minecraft/mc-mods/top-addons"
   }
}
"""

packlock_version1 = """
{
   "extraopt" : {},
   "files" : [
      {
         "location" : "src/"
      }
   ],
   "metadata" : {
      "authors" : [
         "mcrewson"
      ],
      "forge_version" : "14.23.5.2854",
      "minecraft_version" : "1.12.2",
      "name" : "examplepack",
      "title" : "Example Pack",
      "version" : "versionX"
   },
   "resolutions" : {
      "faithful-32x" : {
         "author" : "xMrVizzy",
         "clientonly" : null,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2480/942/Faithful 1.12.2-rv4.zip",
         "fileId" : 2480942,
         "fileName" : "Faithful 1.12.2-rv4.zip",
         "name" : "Faithful x32",
         "optional" : false,
         "projectId" : 236821,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : null,
         "type" : "resourcepack",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/texture-packs/faithful-32x"
      },
      "jei" : {
         "author" : "mezz",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2847/112/jei_1.12.2-4.15.0.293.jar",
         "fileId" : 2847112,
         "fileName" : "jei_1.12.2-4.15.0.293.jar",
         "name" : "Just Enough Items (JEI)",
         "optional" : false,
         "projectId" : 238222,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/jei"
      },
      "journeymap" : {
         "author" : "techbrew",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2916/2/journeymap-1.12.2-5.7.1.jar",
         "fileId" : 2916002,
         "fileName" : "journeymap-1.12.2-5.7.1.jar",
         "name" : "JourneyMap",
         "optional" : false,
         "projectId" : 32274,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/journeymap"
      },
      "mantle" : {
         "author" : "mDiyo",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2713/386/Mantle-1.12-1.3.3.55.jar",
         "fileId" : 2713386,
         "fileName" : "Mantle-1.12-1.3.3.55.jar",
         "name" : "Mantle",
         "optional" : false,
         "projectId" : 74924,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/mantle"
      },
      "the-one-probe" : {
         "author" : "McJty",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2667/280/theoneprobe-1.12-1.4.28.jar",
         "fileId" : 2667280,
         "fileName" : "theoneprobe-1.12-1.4.28.jar",
         "name" : "The One Probe",
         "optional" : false,
         "projectId" : 245211,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/the-one-probe"
      },
      "tinkers-construct" : {
         "author" : "mDiyo",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2902/483/TConstruct-1.12.2-2.13.0.183.jar",
         "fileId" : 2902483,
         "fileName" : "TConstruct-1.12.2-2.13.0.183.jar",
         "name" : "Tinkers Construct",
         "optional" : false,
         "projectId" : 74072,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/tinkers-construct"
      },
      "tinkers-tool-leveling" : {
         "author" : "bonusboni",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2630/860/TinkerToolLeveling-1.12.2-1.1.0.jar",
         "fileId" : 2630860,
         "fileName" : "TinkerToolLeveling-1.12.2-1.1.0.jar",
         "name" : "Tinkers' Tool Leveling",
         "optional" : false,
         "projectId" : 250957,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/tinkers-tool-leveling"
      },
      "top-addons" : {
         "author" : "DrManganese",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2887/479/topaddons-1.12.2-1.13.0.jar",
         "fileId" : 2887479,
         "fileName" : "topaddons-1.12.2-1.13.0.jar",
         "name" : "TOP Addons",
         "optional" : false,
         "projectId" : 247111,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/top-addons"
      }
   },
   "version" : "1"
}
"""

##############################################################################
# THE END
